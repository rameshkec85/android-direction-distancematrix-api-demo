package com.directions;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DistanceMatrix;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.directions.MapKeyConstants.MAP_KEY;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LatLng origin = new LatLng(17.448081, 78.3728713);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        loadData();
    }
    //

    void loadData() {
        final List<String> destinationLatLng = new ArrayList<>();
        destinationLatLng.add(buildLatLngStr(17.3850, 78.4867));
        destinationLatLng.add(buildLatLngStr(17.6868, 83.2185));
        showDestinitionMarkers(destinationLatLng);
    }

    private void showDestinitionMarkers(List<String> destinationLatLng) {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(MAP_KEY)
                .build();

        try {
            DistanceMatrix result = DistanceMatrixApi.getDistanceMatrix(context, new String[]{origin.latitude + "," + origin.longitude}, destinationLatLng.toArray(new String[0])).await();
            //Display origin
            showOriginMarker(origin.latitude, origin.longitude, result.originAddresses[0]);

            long nearestPointDuration = -1;
            int nearestPointIndex = -1;
            for (int i = 0; i < destinationLatLng.size(); i++) {
                String latlngStr = destinationLatLng.get(i);
                String[] latlng = latlngStr.split(",");
                LatLng sydney = new LatLng(Double.parseDouble(latlng[0]), Double.parseDouble(latlng[1]));
                builder.include(sydney);
                String destAddress = result.destinationAddresses[i];
                mMap.addMarker(new MarkerOptions()
                        .position(sydney)
                        .title(destAddress)
                        .snippet(result.rows[0].elements[i].duration.humanReadable)
                );
                //
                long duration = result.rows[0].elements[i].duration.inSeconds;
                if (nearestPointDuration == -1 || nearestPointDuration > duration) {
                    nearestPointDuration = duration;
                    nearestPointIndex = i;
                }
                //
            }
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.10);
            if (!destinationLatLng.isEmpty()) {
                LatLngBounds bounds = builder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                mMap.animateCamera(cu);
            }

            //Find the nearest point and pass lat lng to get directions for it.
            if (nearestPointIndex >= 0) {
                callDirectionApi(destinationLatLng.get(nearestPointIndex));
            }


        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        for (int i = 0; i < destinationLatLng.size(); i++) {
//            String latlngStr = destinationLatLng.get(0);
//            String[] latlng = latlngStr.split(",");
//            LatLng sydney = new LatLng(Double.parseDouble(latlng[0]), Double.parseDouble(latlng[1]));
//            builder.include(sydney);
//            String destAddress = directionMatrixBean.getDestination_addresses().get(i);
//            mMap.addMarker(new MarkerOptions().position(sydney).title(destAddress));
//        }
//        int width = getResources().getDisplayMetrics().widthPixels;
//        int height = getResources().getDisplayMetrics().heightPixels;
//        int padding = (int) (width * 0.10);
//        if (!destinationLatLng.isEmpty()) {
//            LatLngBounds bounds = builder.build();
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//            mMap.animateCamera(cu);
//        }

    }

    void callDirectionApi(String destinationLatLng) {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(MAP_KEY)
                .build();
        try {
            DirectionsResult result = DirectionsApi.getDirections(context, origin.latitude + "," + origin.longitude, destinationLatLng).await();
            List<com.google.maps.model.LatLng> data = result.routes[0].overviewPolyline.decodePath();
            //
            List<LatLng> list = new ArrayList<>();
            //list.add(origin);
            for (com.google.maps.model.LatLng latLng : data) {
                list.add(new LatLng(latLng.lat, latLng.lng));
            }
            //list.add(destination);
            mMap.addPolyline(new PolylineOptions().color(getResources().getColor(R.color.colorPrimary)).addAll(list));
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showOriginMarker(double v, double v1, String s) {
        //
        LatLng sydney = new LatLng(v, v1);
        mMap.addMarker(new MarkerOptions()
                .position(sydney)
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_dot))
                .title(s));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //17.448081,78.3728713
    }

    public String buildLatLngStr(double lat, double lon) {
        return lat + "," + lon;
    }

}
